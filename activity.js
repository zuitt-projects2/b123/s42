let fullNameDisplay = document.querySelector("#full-name-display");
let inputFirstName = document.querySelector("#txt-first-name");
let inputLastName = document.querySelector("#txt-last-name");
let submitBtn = document.querySelector("#submit-btn")

const showName =()=> {

		fullNameDisplay.innerHTML = `${inputFirstName.value}  ${inputLastName.value}`

	}

	inputFirstName.addEventListener('keyup',showName) 
	inputLastName.addEventListener('keyup',showName)
	submitBtn.addEventListener('click', ()=>{

		if(inputFirstName.value === "" || inputLastName.value === ""){
			alert("Please input First name and Last name")
		} else {
			alert(`Thank you for registering ${inputFirstName.value} ${inputLastName.value}`)
			inputLastName.value = ""
			inputFirstName.value= ""
			fullNameDisplay.innerHTML = ""

		}
	})
